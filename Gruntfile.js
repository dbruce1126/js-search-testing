var fs = require('fs');
var jshintOptions = JSON.parse(fs.readFileSync('./.jshintrc'));
module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dist: {
                dest: './dist/app.js',
                src: [
                    './node_modules/jquery/dist/jquery.js',
                    './node_modules/bootstrap/dist/js/bootstrap.js',
                    './node_modules/handlebars/dist/handlebars.js',
                    './assets/*.js'
                ]
            },
            css: {
                dest: './dist/app.css',
                src: [
                    './node_modules/bootstrap/dist/css/bootstrap.css',
                    './node_modules/font-awesome/css/font-awesome.css',
                    './assets/style.css'
                ]
            }
        },
        jshint: {
            options: jshintOptions,
            beforeconcat: [
                './assets/*.js',
            ]
        },
        uglify: {
            options: {
                mangle: true
            },
            dist: {
                files: {
                    './dist/app.js': './dist/app.js',
                }
            },
        },
        connect: {
            server: {
                options: {
                    hostname: '0.0.0.0',
                    port: 8880,
                    livereload: true
                }
            }
        },
        watch: {
            dev: {
                files: [
                    'index.html',
                    'assets/*',
                    '.jshintrc',
                    'bower.json',
                    'Gruntfile.js'
                ],
                tasks: ['jshint:beforeconcat', 'concat:dist', 'concat:css'],
                options: {
                    livereload: true
                }
            }
        }
    });

    // load the npm tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // list of tasks
    grunt.registerTask('build', ['jshint:beforeconcat', 'concat', 'uglify']);
    grunt.registerTask('test', ['jshint:beforeconcat', 'concat']);
    grunt.registerTask('init', ['jshint:beforeconcat', 'concat']);
    grunt.registerTask('dev', ['concat', 'connect:server', 'watch:dev']);
};
