'use strict';
/* globals $,document,Handlebars,ucwords */

var filterFields = [
    'language'
];
var filters = {};

function generateFilters(repositories) {
    var filters = {};
    for (var i = 0; i < filterFields.length; i++) {
        for (var j = 0; j < repositories.length; j++) {
            var filterName = ucwords(filterFields[i].toLowerCase());
            var filterValue = 'None';
            if (repositories[j][filterFields[i]]) {
                filterValue = repositories[j][filterFields[i]];
            }

            if (!filters[filterName]) {
                filters[filterName] = {};
            }
            if (!filters[filterName][filterValue]) {
                filters[filterName][filterValue] = [];
            }
            filters[filterName][filterValue].push(repositories[j]);
        }
    }

    var sortedFilters = {};
    for (var filter in filters) {
        var sortedKeys = Object.keys(filters[filter]).sort();
        sortedFilters[filter] = {};
        for (i = 0; i < sortedKeys.length; i++) {
            sortedFilters[filter][sortedKeys[i]] = filters[filter][sortedKeys[i]];
        }
    }
    return sortedFilters;
}

function updateFilters(repositories) {
    filters = generateFilters(repositories);
    return filters;
}

function bindFilters(target) {
    target.on('change', function(event) {
        var filterName = ucwords($(this).attr('data-filterName'));
        if (filters[filterName]) {
            var filterValue = $(this).attr('data-filterValue');
            if (filters[filterName][filterValue]) {
                var list = filters[filterName][filterValue];
                var selector = list.map(function(repo) {
                    return '#repo_'+repo.id;
                }).join(',');
                if ($(this).is(':checked')) {
                    $(selector).fadeIn('fast');
                    return;
                }
                $(selector).fadeOut('fast');
            }
        }
    });
}
