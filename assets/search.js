'use strict';
/* globals $,document,Handlebars,bindFilters,updateFilters */

var repositories;
var domList, filtersList;
var repoTemplate, filtersTemplate;
var waitingForRequest;

var LANG_IMAGES = {
    c: {
        src: 'http://www.deccansoft.com/Documents/CourseImages/6cdd9d9b-74a6-41aa-8b42-128c3de2a565_C_programming_language.png',
        width: 32,
        height: 32
    },
    css: {
        src: 'https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/256/css.png',
        width: 32,
        height: 32
    },
    haskell: {
        src: 'http://www.seas.upenn.edu/~cis194/spring13/images/haskell-logo-small.png',
        width: 32,
        height: 32
    },
    html: {
        src: 'http://www.w3.org/html/logo/downloads/HTML5_Logo_32.png',
        width: 32,
        height: 32
    },
    javascript: {
        src: 'http://www.b2bweb.fr/wp-content/uploads/js-logo-square-256.png',
        width: 32,
        height: 32
    },
    php: {
        src: 'http://php.net//images/logos/php-icon-white.gif',
        width: 32,
        height: 32
    },
    python: {
        src: 'http://blog.pysoy.org/images/python-square-64.png',
        width: 32,
        height: 32
    },
    shell: {
        src: 'http://1102047360.rsc.cdn77.org/wp-content/uploads/stories/linuxlogos/terminal.png',
        width: 32,
        height: 32
    },
    rust: {
        src: 'http://www.rust-lang.org/logos/rust-logo-32x32.png',
        width: 32,
        height: 32
    }
};

function renderRepositories(target, user, repositories)
{
    waitingForRequest = false;
    if (!repoTemplate) {
        console.log('Unable to retrieve repository template.');
        return;
    }
    if (!target) {
        console.log('Unable to retrieve target DOM element.');
        return;
    }
    target.empty();

    var repoViews = repositories.map(function(repository) {
        var languageImage = (repository.language) ? LANG_IMAGES[repository.language.toLowerCase()] : null;
        return repoTemplate({
            repo: repository,
            username: user,
            langImage: languageImage
        });
    });

    var elements = target.append(repoViews).hide().fadeIn('fast');
    elements.find('img.languageIcon').tooltip();
}

function renderFilters(target, filters)
{
    target.empty();
    target.append(filtersTemplate({filters: filters})).hide().fadeIn('fast');
    bindFilters(target.find('input:checkbox'));
}

function findRepositoriesForUser(user)
{
    var searchUrl = 'https://api.github.com/users/';
    searchUrl += encodeURIComponent(user);
    searchUrl += '/repos?sort=updated';
    $.get(searchUrl, function(repos) {
        repositories = repos;
        renderRepositories(domList, user, repos);
        renderFilters(filtersList, updateFilters(repos));
    });
}

$(document).ready(function() {
    domList = $('#searchResults');
    filtersList = $('#filtersList');
    repoTemplate = Handlebars.compile($('#repoTemplate').html());
    filtersTemplate = Handlebars.compile($('#filtersTemplate').html());
    $('#searchForm').on('submit', function(event) {
        event.preventDefault();
        var username = $('#username').val().trim();
        if (username.length) {
            waitingForRequest = true;
            findRepositoriesForUser(username);
        }
    });
});
